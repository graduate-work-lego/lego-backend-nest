import { IsNotEmpty, IsNotEmptyObject, IsString } from 'class-validator';
import { BlockSettingsType } from '../types/types';

export class CreateBlockDto {
  @IsNotEmpty()
  @IsString()
  type: string;

  @IsNotEmpty()
  @IsString()
  subtype: string;

  @IsNotEmpty()
  @IsNotEmptyObject()
  data: any;

  @IsNotEmpty()
  @IsNotEmptyObject()
  hiddenFields: Record<string, boolean>;

  @IsNotEmpty()
  @IsNotEmptyObject()
  settings: BlockSettingsType;
}
