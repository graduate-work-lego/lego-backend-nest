import { IsInt, IsNotEmpty } from 'class-validator';
import { CreateBlockDto } from './create-block.dto';

export class UpdateBlockDto extends CreateBlockDto {
  @IsInt()
  @IsNotEmpty()
  id: number;
}
