import { Module } from '@nestjs/common';
import { BlockService } from './block.service';
import { BlockController } from './block.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Block } from './entities/block.entity';
import { PageModule } from '../page/page.module';

@Module({
  imports: [TypeOrmModule.forFeature([Block]), PageModule],
  controllers: [BlockController],
  providers: [BlockService],
})
export class BlockModule {}
