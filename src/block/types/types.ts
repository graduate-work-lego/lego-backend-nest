export type BlockSettingsType = {
  isHidden: boolean;
  background: BlockBackgroundType;
};

export type BlockBackgroundType = {
  colorScheme: ColorScheme;
  color: string;
};

export enum ColorScheme {
  WHITE = 'WHITE',
  BLACK = 'BLACK',
}
