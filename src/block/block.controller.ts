import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
} from '@nestjs/common';
import { BlockService } from './block.service';
import { CreateBlockDto } from './dto/create-block.dto';
import { UpdateBlockDto } from './dto/update-block.dto';

@Controller('/blocks')
export class BlockController {
  constructor(private readonly blockService: BlockService) {}

  @Post('/create/for-page/:pageId')
  create(
    @Param('pageId') pageId: number,
    @Body() createBlockDto: CreateBlockDto,
  ) {
    return this.blockService.createForPage(createBlockDto, pageId);
  }

  @Get()
  findAll() {
    return this.blockService.findAll();
  }

  @Delete(':blockId')
  remove(@Param('blockId') blockId: number) {
    return this.blockService.remove(blockId);
  }

  @Put()
  update(@Body() updateBlockDto: UpdateBlockDto) {
    return this.blockService.update(updateBlockDto);
  }
}
