import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBlockDto } from './dto/create-block.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Block } from './entities/block.entity';
import { Repository } from 'typeorm';
import { PageService } from '../page/page.service';
import { Page } from '../page/entities/page.entity';
import { UpdateBlockDto } from './dto/update-block.dto';

@Injectable()
export class BlockService {
  constructor(
    @InjectRepository(Block) private blockRepository: Repository<Block>,
    private pageService: PageService,
  ) {}

  async createForPage(createBlockDto: CreateBlockDto, pageId: number) {
    const page: Page = await this.pageService.getById(pageId);
    await this.blockRepository.save({ ...createBlockDto, page });
    return this.findAllByPageId(pageId);
  }

  findAll() {
    return `This action returns all block`;
  }

  findAllByPageId(pageId: number) {
    return this.blockRepository.find({
      where: {
        page: {
          id: pageId,
        },
      },
    });
  }

  findOne(id: number) {
    return `This action returns a #${id} block`;
  }

  async remove(id: number) {
    await this.blockRepository.delete(id);
    return true;
  }

  async update(updateBlockDto: UpdateBlockDto) {
    const existedBlock = await this.blockRepository.findOneBy({
      id: updateBlockDto.id,
    });

    console.log(existedBlock);

    if (!existedBlock) {
      throw new NotFoundException();
    }

    return this.blockRepository.save(updateBlockDto);
  }
}
