import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../utils/base.entity';
import { IsNotEmpty, IsString } from 'class-validator';
import { Page } from '../../page/entities/page.entity';
import { BlockSettingsType } from '../types/types';

@Entity('block')
export class Block extends BaseEntity {
  @IsNotEmpty()
  @IsString()
  @Column()
  type: string;

  @IsNotEmpty()
  @IsString()
  @Column()
  subtype: string;

  @IsNotEmpty()
  @Column({
    type: 'jsonb',
  })
  data: any;

  @IsNotEmpty()
  @Column({
    type: 'jsonb',
  })
  hiddenFields: Record<string, boolean>;

  @IsNotEmpty()
  @Column({
    type: 'jsonb',
  })
  settings: BlockSettingsType;

  @ManyToOne(() => Page, (page) => page.blocks)
  page: Page;
}
