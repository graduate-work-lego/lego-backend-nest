import { Controller, Post, Body, Get, Param } from '@nestjs/common';
import { PageService } from './page.service';
import { CreatePageDto } from './dto/create-page.dto';
import { Auth } from '../auth/decorators/auth.decorator';
import { CurrentUser } from '../auth/decorators/user.decorator';

@Controller('/pages')
export class PageController {
  constructor(private readonly pageService: PageService) {}

  @Post('/create')
  @Auth()
  create(@Body() createPageDto: CreatePageDto) {
    return this.pageService.create(createPageDto);
  }

  @Get('/')
  getAll() {
    return this.pageService.getAll();
  }

  @Get('/:pageId')
  getById(@Param('pageId') pageId: number) {
    return this.pageService.getById(pageId);
  }
}
