import { Injectable } from '@nestjs/common';
import { CreatePageDto } from './dto/create-page.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Page } from './entities/page.entity';
import { Repository } from 'typeorm';
import { Site } from '../site/entities/site.entity';
import { SiteService } from '../site/site.service';
import { getGenerateRandomString } from '../utils/utils';

@Injectable()
export class PageService {
  constructor(
    @InjectRepository(Page) private pageRepository: Repository<Page>,
    private siteService: SiteService,
  ) {}

  async create(createPageDto: CreatePageDto): Promise<Page> {
    const site: Site = await this.siteService.findById(createPageDto.siteId);
    const allSitePages = await this.getAllBySiteId(site.id);
    const pageName = `Страница ${allSitePages.length ? ++allSitePages.length : 1}`;
    return this.pageRepository.save({
      site,
      title: pageName,
      code: getGenerateRandomString(),
    });
  }

  async getAll(): Promise<Page[]> {
    return this.pageRepository.find();
  }

  async getAllBySiteId(siteId: number): Promise<Page[]> {
    return this.pageRepository.findBy({
      site: {
        id: siteId,
      },
    });
  }

  async getById(id: number) {
    return this.pageRepository.findOneBy({
      id,
    });
  }
}
