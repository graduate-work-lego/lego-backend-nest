import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from '../../utils/base.entity';
import { IsString } from 'class-validator';
import { Site } from '../../site/entities/site.entity';
import { JoinColumn } from 'typeorm';
import { Block } from '../../block/entities/block.entity';

@Entity('page')
export class Page extends BaseEntity {
  @IsString()
  @Column({ nullable: false })
  title: string;

  @IsString()
  @Column({ nullable: false })
  code: string;

  @ManyToOne(() => Site, (site) => site.pages)
  @JoinColumn({
    name: 'site_id',
  })
  site: Site;

  @OneToMany(() => Block, (block) => block.page, {
    cascade: true,
    eager: true,
  })
  blocks: Block[];
}
