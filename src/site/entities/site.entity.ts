import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { IsString } from 'class-validator';
import { BaseEntity } from '../../utils/base.entity';
import { Page } from '../../page/entities/page.entity';
import { User } from '../../user/entities/user.entity';

@Entity('site')
export class Site extends BaseEntity {
  @IsString()
  @Column({ nullable: false })
  name: string;

  @IsString()
  @Column()
  code: string;

  @OneToMany(() => Page, (page) => page.site, {
    cascade: true,
    eager: true,
  })
  pages: Page[];

  @ManyToOne(() => User, (user) => user.sites)
  user: User;
}
