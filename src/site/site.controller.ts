import { Controller, Get, Param, Post } from '@nestjs/common';
import { SiteService } from './site.service';
import { Auth } from '../auth/decorators/auth.decorator';
import { CurrentUser } from '../auth/decorators/user.decorator';

@Controller('/sites')
export class SiteController {
  constructor(private readonly siteService: SiteService) {}

  @Post('/create')
  @Auth()
  create(@CurrentUser('id') userId: number) {
    return this.siteService.create(userId);
  }

  @Get('/')
  @Auth()
  getAll(@CurrentUser('id') userId: number) {
    return this.siteService.getAllByUserId(userId);
  }

  @Get('/:siteId')
  getById(@Param('siteId') siteId: number) {
    return this.siteService.findById(siteId);
  }
}
