import { Injectable } from '@nestjs/common';
import { Site } from './entities/site.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { getGenerateRandomString } from '../utils/utils';
import { UserService } from '../user/user.service';

@Injectable()
export class SiteService {
  constructor(
    @InjectRepository(Site)
    private siteRepository: Repository<Site>,
    private userService: UserService,
  ) {}

  async create(userId: number): Promise<Site> {
    const user = await this.userService.getById(userId);
    const allUserSites = await this.getAllByUserId(userId);
    const siteName = `Сайт ${allUserSites.length ? ++allUserSites.length : 1}`;
    return this.siteRepository.save({
      user,
      code: getGenerateRandomString(),
      name: siteName,
    });
  }

  async getAllByUserId(userId: number): Promise<Site[]> {
    return this.siteRepository.find({
      relations: {
        user: true,
      },
      where: {
        user: {
          id: userId,
        },
      },
    });
  }

  findById(id: number) {
    return this.siteRepository.findOneBy({
      id,
    });
  }
}
