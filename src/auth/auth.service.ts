import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { UserService } from '../user/user.service';
import * as bcrypt from 'bcrypt';
import { User } from '../user/entities/user.entity';
import { AuthRequestDto } from './dto/auth-request.dto';
import { JwtService } from '@nestjs/jwt';
import { AuthResponseDto } from './dto/auth-response.dto';
import { UserDTO } from '../user/dto/user.dto';
import { Response } from 'express';

@Injectable()
export class AuthService {
  readonly EXPIRE_DAY_REFRESH_TOKEN = 25;
  readonly REFRESH_TOKEN_NAME = 'refreshToken';

  constructor(
    private userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async login(dto: AuthRequestDto) {
    const user: User = await this.validateUser(dto.login, dto.password);
    const { accessToken, refreshToken } = this.issueToken(user.id);
    return new AuthResponseDto(
      new UserDTO(UserDTO.builder().withLogin(user.login).withId(user.id)),
      accessToken,
      refreshToken,
    );
  }

  async register(dto: AuthRequestDto) {
    const userDto: UserDTO = await this.userService.create(dto);
    const { accessToken, refreshToken } = this.issueToken(userDto.id);
    return new AuthResponseDto(userDto, accessToken, refreshToken);
  }

  async getNewTokens(refreshToken: string): Promise<AuthResponseDto> {
    const result = await this.jwtService.verifyAsync(refreshToken);
    if (!result) {
      throw new UnauthorizedException('Invalid refresh token');
    }

    const userDto: UserDTO = await this.userService.getById(result.id);

    const tokens = this.issueToken(userDto.id);

    return new AuthResponseDto(
      userDto,
      tokens.accessToken,
      tokens.refreshToken,
    );
  }

  private issueToken(userId: number) {
    const data = { id: userId };
    const accessToken = this.jwtService.sign(data, {
      expiresIn: '1h',
    });

    const refreshToken = this.jwtService.sign(data, {
      expiresIn: `${this.EXPIRE_DAY_REFRESH_TOKEN}d`,
    });

    return {
      accessToken,
      refreshToken,
    };
  }

  addRefreshTokenToResponse(res: Response, refreshToken: string) {
    const expiresIn = new Date();
    expiresIn.setDate(expiresIn.getDate() + this.EXPIRE_DAY_REFRESH_TOKEN);

    res.cookie(this.REFRESH_TOKEN_NAME, refreshToken, {
      httpOnly: true,
      domain: 'localhost',
      expires: expiresIn,
      secure: true,
      sameSite: 'none',
    });
  }

  removeRefreshTokenFromResponse(res: Response) {
    const expiresIn = new Date();
    expiresIn.setDate(expiresIn.getDate() + this.EXPIRE_DAY_REFRESH_TOKEN);

    res.cookie(this.REFRESH_TOKEN_NAME, '', {
      httpOnly: true,
      domain: 'localhost',
      expires: new Date(0),
      secure: true,
      sameSite: 'none',
    });
  }

  private async validateUser(
    login: string,
    pass: string,
  ): Promise<User | null> {
    const user = await this.userService.findOne(login);
    if (user) {
      const passwordIsMatch = await bcrypt.compare(pass, user.password);
      if (!passwordIsMatch) {
        throw new UnauthorizedException('Пароль неверный');
      }

      return user;
    } else {
      throw new NotFoundException('Пользователь с таким логином не найден');
    }
  }
}
