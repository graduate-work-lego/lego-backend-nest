import { UserDTO } from '../../user/dto/user.dto';

export class AuthResponseDto {
  user: UserDTO;
  accessToken: string;
  refreshToken: string;

  constructor(user: UserDTO, accessToken: string, refreshToken: string) {
    this.user = user;
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
  }
}
