import { IsString, MinLength } from 'class-validator';

export class AuthRequestDto {
  @IsString()
  login: string;

  @MinLength(3, {
    message: 'Пароль должен состоять минимум из 3 символов',
  })
  @IsString()
  password: string;
}
