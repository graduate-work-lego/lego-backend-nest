import { BadRequestException, Injectable } from '@nestjs/common';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UserDTO } from './dto/user.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}

  async getAll(): Promise<User[]> {
    return await this.userRepository.find();
  }

  async create(createUserDto: CreateUserDto) {
    const existedUser: User = await this.findOne(createUserDto.login);

    if (existedUser) {
      throw new BadRequestException(
        'Пользователь с таким логином уже существует',
      );
    }
    const user = await this.userRepository.save({
      ...createUserDto,
      password: await bcrypt.hash(createUserDto.password, 1),
    });
    return new UserDTO(UserDTO.builder().withId(user.id).withLogin(user.login));
  }

  async getById(id: number): Promise<UserDTO> {
    const user = await this.userRepository.findOneBy({
      id,
    });

    return new UserDTO(UserDTO.builder().withId(user.id).withLogin(user.login));
  }

  async findOne(login: string): Promise<User | undefined> {
    return this.userRepository.findOneBy({
      login,
    });
  }
}
