import { Column, Entity, OneToMany } from 'typeorm';
import { BaseEntity } from '../../utils/base.entity';
import { IsString } from 'class-validator';
import { Site } from '../../site/entities/site.entity';

@Entity('user')
export class User extends BaseEntity {
  @IsString()
  @Column({
    type: 'varchar',
  })
  login: string;

  @IsString()
  @Column()
  password: string;

  @OneToMany(() => Site, (site) => site.user, {
    cascade: true,
  })
  sites: Site[];
}
