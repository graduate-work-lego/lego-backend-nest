export class UserDTO {
  readonly id: number;
  readonly login: string;

  constructor(builder: UserDTOBuilder) {
    this.id = builder.id;
    this.login = builder.login;
  }

  static builder(): UserDTOBuilder {
    return new UserDTOBuilder();
  }
}

class UserDTOBuilder {
  private _id: number;
  private _login: string;

  constructor() {}

  withId(id: number): UserDTOBuilder {
    this._id = id;
    return this;
  }

  withLogin(login: string): UserDTOBuilder {
    this._login = login;
    return this;
  }

  get id(): number {
    return this._id;
  }

  get login(): string {
    return this._login;
  }
}
