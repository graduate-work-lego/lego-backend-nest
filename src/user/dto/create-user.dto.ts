import { IsString } from 'class-validator';

export class CreateUserDto {
  @IsString()
  readonly login: string;
  @IsString()
  readonly password: string;

  constructor(login: string, password: string) {
    this.login = login;
    this.password = password;
  }
}
